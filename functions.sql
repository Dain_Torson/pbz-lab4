CREATE OR REPLACE FUNCTION get_schedule(dep integer)
  RETURNS TABLE(name char(40), ulevel smallint, llevel smallint, pamount int) AS $BODY$
BEGIN
  RETURN QUERY SELECT post.post_name, post.up_level, post.low_level, sdepart.amount FROM (department INNER JOIN schedule ON
  	department.schedule_id = schedule.schedule_id ) sdepart INNER JOIN post ON sdepart.post_name = post.post_name
  	WHERE dep_id = $1;
END;
$BODY$
LANGUAGE plpgsql

CREATE OR REPLACE FUNCTION get_elders()
  RETURNS SETOF employee AS $BODY$
BEGIN
  RETURN QUERY SELECT * FROM employee
  	WHERE age >= 55 AND gender = 'жен';
END;
$BODY$
LANGUAGE plpgsql

CREATE OR REPLACE FUNCTION get_employes(maxage int, tpost char(40))
  RETURNS SETOF employee AS $BODY$
BEGIN
  RETURN QUERY SELECT * FROM employee
  	WHERE age < $1 AND post_name = $2;
END;
$BODY$
LANGUAGE plpgsql

CREATE OR REPLACE FUNCTION get_id(tname char(40))
  RETURNS integer AS $BODY$
DECLARE eid INTEGER;
BEGIN
  SELECT id INTO eid FROM employee WHERE fio = tname;
  RETURNS eid;
END;
$BODY$
LANGUAGE plpgsql

CREATE OR REPLACE FUNCTION add_hist(tname char(40), place int, tpost char(40), lvl integer, stdate date, endate date)
  RETURNS void AS $BODY$
DECLARE eid INTEGER;
BEGIN
  SELECT * INTO eid FROM get_id($1);
  INSERT INTO employee_hist (emp_id, wplace, post_name, level, sdate, edate)
   VALUES (eid, place, tpost, lvl, stdate, endate);
  RETURN;
END;
$BODY$
LANGUAGE plpgsql

CREATE OR REPLACE FUNCTION get_hist(
    IN tname character,
    IN startdate date)
  RETURNS TABLE(place integer, tpost char(40), lvl integer, stdate date, endate date) AS
$BODY$
DECLARE eid INTEGER;
BEGIN
  SELECT * INTO eid FROM get_id($1);
  RETURN QUERY SELECT wplace, post_name, level, sdate, edate FROM employee_hist WHERE emp_id = eid AND sdate = $2;
END;
$BODY$
  LANGUAGE plpgsql

CREATE OR REPLACE FUNCTION delete_hist(tname char(40), tdate date)
  RETURNS void AS $BODY$
DECLARE eid INTEGER;
BEGIN
  SELECT * INTO eid FROM get_id($1);
  DELETE FROM employee_hist WHERE emp_id = eid AND sdate = tdate;
  RETURN;
END;
$BODY$
LANGUAGE plpgsql

CREATE OR REPLACE FUNCTION update_hist(
    tname character,
    place integer,
    tpost char(40),
    lvl integer,
    stdate date,
    endate date)
  RETURNS void AS
$BODY$
DECLARE eid INTEGER;
BEGIN
  SELECT * INTO eid FROM get_id($1);
  UPDATE employee_hist SET wplace = place, level = lvl, sdate = stdate, edate = endate, post_name = tpost
   WHERE emp_id = eid AND sdate = stdate;
  RETURN;
END;
$BODY$
  LANGUAGE plpgsql


CREATE OR REPLACE FUNCTION delete_all_hist(tname char(40))
  RETURNS void AS $BODY$
DECLARE eid INTEGER;
BEGIN
  SELECT * INTO eid FROM get_id($1);
  DELETE FROM employee_hist WHERE emp_id = eid;
  RETURN;
END;
$BODY$
LANGUAGE plpgsql



